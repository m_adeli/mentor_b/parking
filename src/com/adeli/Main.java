package com.adeli;


public class Main {

    public static void main(String[] args) {
        Parking p = new Parking(10);
        p.addCar("12U967", "red");
        p.addCar("98R345", "black");
        p.addCar("56F115", "green");
        p.addCar("32Q870", "white");
        p.addCar("90Y334", "blue");
        p.addCar("30Z665", "yellow");
        p.addCar("13W765", "black");
        p.addCar("93O305", "red");
        p.addCar("85P225", "red");
        p.addCar("11A377", "black");

        p.calculatePrice("12U967");
        p.calculatePrice("11A377");
        p.calculatePrice("85P225");
        p.calculatePrice("56F115");

        p.getIncomeList().forEach(System.out::println);
    }
}
