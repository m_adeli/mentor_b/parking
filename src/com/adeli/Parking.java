package com.adeli;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.time.temporal.ChronoUnit.MINUTES;

public class Parking {
    private int capacity;
    private int carsNumber;
    private final int entryPrice = 5000;
    private final int fixTime = 240;
    private final int penaltyPerMinute = 900 / 60; // each 1 hour is 900
    private List<ParkingCard> parkingCards;
    private List<Agent> agents;
    private List<List> incomeList;

    public Parking(int capacity) {
        this.capacity = capacity;
        parkingCards = new ArrayList<>();
        agents = new ArrayList<>();
        incomeList = new ArrayList<>();
        agents.add(new Agent("Ali", LocalTime.of(0, 1), LocalTime.of(12, 0)));
        agents.add(new Agent("Hadi", LocalTime.of(12, 1), LocalTime.of(0, 0)));
    }

    public void addCar(String licensePlate, String color) {
        if (!isFull()) {
            parkingCards.add(new ParkingCard(licensePlate, color, getCurrentAgent().getName()));
            carsNumber++;
        } else
            System.out.println("Parking is full!");
    }

    public void calculatePrice(String licensePlate) {
        LocalTime exitTime = LocalTime.of(11, 20);
//        LocalTime exitTime = LocalTime.now();
        ParkingCard pc = parkingCards.stream().filter(x -> {
            if (x.getLicensePlate().equals(licensePlate))
                return true;
            return false;
        }).collect(Collectors.toList()).get(0);
        long duration = MINUTES.between(pc.getEntryTime(), exitTime);
        int price = 0;
        if (duration > fixTime) {
            price = (int) (entryPrice + ((duration - fixTime) * penaltyPerMinute));
        }
        List x = Arrays.asList(pc, getCurrentAgent(), exitTime, price);
        incomeList.add(x);
        parkingCards.remove(pc);
        carsNumber--;
    }

    public long totalIncome() {
        long total = incomeList.stream().mapToInt(x -> (int) x.get(3)).sum();
        return total;
    }

    public Agent getCurrentAgent() {
        Agent result = null;
        for (Agent a : agents) {
            boolean x = a.getStart().isBefore(LocalTime.now());
            boolean y = a.getEnd().isAfter(LocalTime.now());
            if (x && y) {
                result = a;
                break;
            }
        }
        return result;
    }

    private boolean isFull() {
        return carsNumber == capacity;
    }

    public List<ParkingCard> getParkingCards() {
        return parkingCards;
    }

    public void setParkingCards(List<ParkingCard> parkingCards) {
        this.parkingCards = parkingCards;
    }

    public List<Agent> getAgents() {
        return agents;
    }

    public void setAgents(List<Agent> agents) {
        this.agents = agents;
    }

    public List<List> getIncomeList() {
        return incomeList;
    }
}
