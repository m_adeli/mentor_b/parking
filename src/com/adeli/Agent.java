package com.adeli;

import java.time.LocalTime;
import java.util.Locale;

public class Agent {
    private String name;
    private LocalTime start;
    private LocalTime end;

    public Agent(String name, LocalTime start, LocalTime end) {
        this.name = name;
        this.start = start;
        this.end = end;
    }

    public String getName() {
        return name;
    }

    public LocalTime getStart() {
        return start;
    }

    public LocalTime getEnd() {
        return end;
    }
}
