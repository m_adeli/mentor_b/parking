package com.adeli;

import java.time.LocalTime;

public class ParkingCard {
    private String color;
    private String licensePlate;
    private String agentName;
    private LocalTime entryTime;

    public ParkingCard(String licensePlate, String color, String agentName) {
        this.color = color;
        this.licensePlate = licensePlate;
        this.agentName = agentName;
        this.entryTime = LocalTime.now();
    }

    public String getColor() {
        return color;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public String getAgentName() {
        return agentName;
    }

    public LocalTime getEntryTime() {
        return entryTime;
    }

    @Override
    public String toString() {
        return "ParkingCard{" +
                "color='" + color + '\'' +
                ", licensePlate='" + licensePlate + '\'' +
                ", agentName='" + agentName + '\'' +
                ", entryTime=" + entryTime +
                '}';
    }
}
